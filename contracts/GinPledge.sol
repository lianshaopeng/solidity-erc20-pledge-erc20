pragma solidity 0.5.8;

import "./SafeMath.sol";
import "./SafeERC20.sol";

contract GinPledge {
    using SafeMath for uint256;
    using SafeERC20 for ERC20;

    address private owner;
    address private profitor;
    bool _isDIS = false;

    mapping(address => PledgeOrder) _orders;
    mapping(address => uint256) _takeProfitTime;

    ERC20 _Token;
    ERC20 _StorageToken; //需要质押的erc20,无法正常使用TODO
    KeyFlag[] keys;

    uint256 size;
    uint256 _maxPledgeAmount; //最大质押数量
    uint256 _maxMiningAmount; //最大收益分配额度
    uint256 _leftMiningAmount; //剩余收益分配额度
    uint256 _minAmount; //最小质押数量
    uint256 _totalPledegAmount; //总质押数量
    uint256 _maxPreMiningAmount; //每次收益分配额度
    uint256 _startTime; //质押开始时间
    uint256 _endTime; //质押结束时间
    uint256 _precentUp = 100;
    uint256 _precentDown = 100;
    address _pledgeToken; //需要质押的合约地址，一旦确定无法修改

    event TransferFrom(address indexed from, address indexed to, uint256 value);

    //质押的订单
    struct PledgeOrder {
        bool isExist; //是否存在
        uint256 token; //质押的数量
        uint256 profitToken; //奖励数量
        uint256 time; //修改订单时间
        uint256 index; //订单索引位置
    }

    struct KeyFlag {
        address key;
        bool isExist;
    }

    constructor(
        address tokenAddress,
        address paramProfitor, //奖励erc20部署者的钱包地址
        uint256 maxPledgeAmount,
        uint256 minAmount,
        uint256 maxMiningAmount,
        uint256 maxPreMiningAmount,
        uint256 startTime,
        uint256 endTime,
        address pledgeToken
    ) public {
        _Token = ERC20(tokenAddress);
        _StorageToken = ERC20(pledgeToken);
        owner = msg.sender;
        profitor = paramProfitor;
        _maxPledgeAmount = maxPledgeAmount;
        _minAmount = minAmount;
        _maxMiningAmount = maxMiningAmount;
        _maxPreMiningAmount = maxPreMiningAmount;
        _startTime = startTime;
        _endTime = endTime;
        _leftMiningAmount = maxMiningAmount;
        _pledgeToken = pledgeToken;
    }

    //LianSP:质押20
    function pledgeToken(uint256 value) public {
        require(value > 0, "less token");
        require(address(msg.sender) == address(tx.origin), "no contract");
        require(_isDIS, "is disable");
        require(_leftMiningAmount > 0, "less token");
        //总质押数量要小于最大质押数量
        require(
            _totalPledegAmount.add(value) <= _maxPledgeAmount,
            "more token"
        );
        require(
            block.timestamp >= _startTime && block.timestamp <= _endTime,
            "is disable"
        );
        //判断转账地址余额是否充足
        uint256 msgSenderBalance = _StorageToken.safeBalanceOf(
            _pledgeToken,
            address(msg.sender)
        );
        require(msgSenderBalance >= value, "token is not enough");
        // 修改质押的映射
        if (_orders[msg.sender].isExist == false) {
            //首次参与质押
            keys.push(KeyFlag(msg.sender, true));
            size++;
            createOrder(value, keys.length.sub(1));
        } else {
            PledgeOrder storage order = _orders[msg.sender];
            order.token = order.token.add(value);
            keys[order.index].isExist = true;
        }
        //调用erc20转账到合约（调用erc20的方法)
        _StorageToken.safeTransferFrom(msg.sender, address(this), value);
        emit TransferFrom(msg.sender, address(this), value);
        _totalPledegAmount = _totalPledegAmount.add(value);
    }

    //创建订单
    function createOrder(uint256 trcAmount, uint256 index) private {
        _orders[msg.sender] = PledgeOrder(
            true,
            trcAmount,
            0,
            block.timestamp,
            index
        );
    }

    //分发奖励
    function profit() public onlyProfitor {
        require(_leftMiningAmount > 0, "less token");
        require(_totalPledegAmount > 0, "no pledge");
        uint256 preToken = _maxPreMiningAmount;
        if (_leftMiningAmount < _maxPreMiningAmount) {
            preToken = _leftMiningAmount;
        }
        for (uint256 i = 0; i < keys.length; i++) {
            if (keys[i].isExist == true) {
                PledgeOrder storage order = _orders[keys[i].key];
                order.profitToken = order.profitToken.add(
                    order.token.mul(preToken).div(_totalPledegAmount)
                );
            }
        }
        _leftMiningAmount = _leftMiningAmount.sub(preToken);
    }

    //获取分发奖励
    function takeProfit() public {
        require(address(msg.sender) == address(tx.origin), "no contract");
        require(_orders[msg.sender].profitToken > 0, "less token");
        uint256 time = block.timestamp;
        uint256 diff = time.sub(_takeProfitTime[msg.sender]);
        require(diff > 86400, "less time");
        PledgeOrder storage order = _orders[msg.sender];
        uint256 takeToken = order.profitToken.mul(_precentUp).div(_precentDown);
        order.profitToken = order.profitToken.sub(takeToken);
        _takeProfitTime[msg.sender] = time;
        _Token.safeTransfer(address(msg.sender), takeToken); //从合约中往指定地址转账
    }

    //用户取回质押币
    function takeToken(uint256 amount) public {
        require(address(msg.sender) == address(tx.origin), "no contract");
        PledgeOrder storage order = _orders[msg.sender];
        require(order.token > 0, "no order");
        require(amount <= order.token, "less token");
        _totalPledegAmount = _totalPledegAmount.sub(amount);
        if (order.token == amount) {
            order.token = 0;
            keys[order.index].isExist = false;
        } else {
            order.token = order.token.sub(amount);
        }
        _StorageToken.safeTransfer(address(msg.sender), amount); //LianSP
    }

    function takeAllToken() public {
        require(address(msg.sender) == address(tx.origin), "no contract");
        PledgeOrder storage order = _orders[msg.sender];
        require(order.token > 0, "no order");
        keys[order.index].isExist = false;
        uint256 takeAmount = order.token;
        order.token = 0;
        _totalPledegAmount = _totalPledegAmount.sub(takeAmount);
        uint256 time = block.timestamp;
        uint256 diff = time.sub(_takeProfitTime[msg.sender]);
        if (diff >= 86400) {
            uint256 profitPart = order.profitToken.mul(_precentUp).div(
                _precentDown
            );
            keys[order.index].isExist = false;
            order.profitToken = order.profitToken.sub(profitPart);
            _takeProfitTime[msg.sender] = time;
            _Token.safeTransfer(address(msg.sender), profitPart);
        }
        _StorageToken.safeTransfer(address(msg.sender), takeAmount); //LianSP
    }

    // 本次增加内容
    function withdrawProfit(uint256 amount) public onlyProfitor {
        _Token.safeTransfer(address(msg.sender), amount);
    }

    //获取质押的总数量
    function getPledgeToken(address tokenAddress)
        public
        view
        returns (uint256)
    {
        require(address(msg.sender) == address(tx.origin), "no contract");
        PledgeOrder memory order = _orders[tokenAddress];
        return order.token;
    }

    function getProfitToken(address tokenAddress)
        public
        view
        returns (uint256)
    {
        require(address(msg.sender) == address(tx.origin), "no contract");
        PledgeOrder memory order = _orders[tokenAddress];
        return order.profitToken;
    }

    function getTotalPledge() public view returns (uint256) {
        require(address(msg.sender) == address(tx.origin), "no contract");
        return _totalPledegAmount;
    }

    function getTakeProfitTime(address tokenAddress)
        public
        view
        returns (uint256)
    {
        return _takeProfitTime[tokenAddress];
    }

    function changeIsDIS(bool flag) public onlyOwner {
        _isDIS = flag;
    }

    function changeOwner(address paramOwner) public onlyOwner {
        require(paramOwner != address(0));
        owner = paramOwner;
    }

    function changeProfitor(address paramProfitor) public onlyOwner {
        profitor = paramProfitor;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    modifier onlyProfitor() {
        require(msg.sender == profitor);
        _;
    }

    function getOwner() public view returns (address) {
        return owner;
    }

    function getProfitor() public view returns (address) {
        return profitor;
    }

    function getsize() public view returns (uint256) {
        return size;
    }

    function maxPledgeAmount() public view returns (uint256) {
        return _maxPledgeAmount;
    }

    function maxMiningAmount() public view returns (uint256) {
        return _maxMiningAmount;
    }

    function leftMiningAmount() public view returns (uint256) {
        return _leftMiningAmount;
    }

    function minAmount() public view returns (uint256) {
        return _minAmount;
    }

    function maxPreMiningAmount() public view returns (uint256) {
        return _maxPreMiningAmount;
    }

    function startTime() public view returns (uint256) {
        return _startTime;
    }

    function endTime() public view returns (uint256) {
        return _endTime;
    }

    function nowTime() public view returns (uint256) {
        return block.timestamp;
    }

    function isDIS() public view returns (bool) {
        return _isDIS;
    }
}
