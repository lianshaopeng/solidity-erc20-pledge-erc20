/*
 * @Descripttion:
 * @version: 1.0
 * @Author: LianSP
 * @Date: 2021-10-09 15:01:00
 * @LastEditors: LianSP
 * @LastEditTime: 2021-10-15 17:02:07
 * @Company: MOAC
 */
const GinPledge = artifacts.require("GinPledge");
const GinsengToken20A = artifacts.require("GinsengToken20");
const GinsengToken20B = artifacts.require("GinsengToken20B");

module.exports = async function (deployer) {
    await deployer.deploy(GinsengToken20A, 123456789, "aaaa", 18, "aa").then(async (aaa) => {
        console.log("初始a：", aaa.address);
        console.log("初始部署者：", aaa.constructor.class_defaults.from);
        await deployer.deploy(GinsengToken20B, 987654321, "bbbb", 18, "bb").then(async (bbb) => {
            console.log("初始b:", bbb.address);
            await deployer
                .deploy(
                    GinPledge,
                    aaa.address,
                    aaa.constructor.class_defaults.from,
                    "100000000",
                    "10000000",
                    "100000000",
                    "10000000",
                    1634023662,
                    1936702062,
                    bbb.address,
                )
                .then(async (instance) => {
                    console.log("初始质押合约地址：", instance.address);
                    //测试地址
                    let firstAddress = "0x4DcBd7c365D7b0b6b5ddC70c9A30355f24899066";
                    //转质押币给测试钱包地址
                    await bbb.transfer(firstAddress, 12300000000000);
                    //转账到质押合约用于奖励使用
                    await aaa.transfer(instance.address, 10000000000000);
                    console.log("质押合约拥有的奖励币种的余额：", String(await aaa.balanceOf.call(instance.address)));
                    //开启质押合约
                    await instance.changeIsDIS(true);
                    console.log("质押合约开启状态:", await instance.isDIS.call());

                    let pledgeTokenB = 11100000;

                    //对质押合约进行授权用于质押
                    await bbb.approve(instance.address, pledgeTokenB, {
                        from: firstAddress,
                    });
                    //进行质押
                    await instance.pledgeToken(pledgeTokenB, {
                        from: firstAddress,
                    });

                    //查询质押的总数量
                    console.log(
                        "从质押合约查询到的质押的总数量：",
                        String(await instance.getPledgeToken.call(firstAddress)),
                    );
                    //查询erc20余额查询到的b的erc20数量
                    console.log(
                        "从erc20合约查询到的质押的总数量：",
                        String(await bbb.balanceOf.call(instance.address)),
                    );

                    //进行分发奖励
                    await instance.profit();
                    //用户提取奖励的质押币
                    console.log("用户提取之前的奖励币数量:", String(await aaa.balanceOf(firstAddress)));
                    await instance.takeProfit({ from: firstAddress });
                    console.log("用户提取之后的奖励币数量：", String(await aaa.balanceOf(firstAddress)));

                    //用户取回质押的token
                    await instance.takeToken(12345, { from: firstAddress });
                    console.log("提取质押币后用户质押数量:", String(await bbb.balanceOf.call(instance.address)));
                    console.log("用户提取质押币后质押币的数量：", String(await bbb.balanceOf.call(firstAddress)));
                });
        });
    });
};
