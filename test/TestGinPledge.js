/*
 * @Descripttion:
 * @version: 1.0
 * @Author: LianSP
 * @Date: 2021-10-12 16:13:21
 * @LastEditors: LianSP
 * @LastEditTime: 2021-10-15 15:34:41
 * @Company: MOAC
 */
const GinPledge = artifacts.require("GinPledge");
const GinsengToken20A = artifacts.require("GinsengToken20");
const GinsengToken20B = artifacts.require("GinsengToken20B");

/**
 * TODO:使用truffler test 测试文件，会出现部署的合约是migrations中已经生成好的合约，当多合约互相嵌套使用会出现问题。
 */

contract("test GinPledge", async (accounts) => {
    it("set msg", async () => {
        const erc20InstanceA = await GinsengToken20A.deployed(123456789, "aaaa", 18, "aa");
        const erc20InstanceB = await GinsengToken20B.deployed(123456789, "bbbb", 18, "bb");
        //质押合约待分发的erc20的合约地址
        // const erc20InstanceA = 0x310318e71a6cf8c0e81ac4ac170ea3e73d5c48d2;
        // //质押合约需要质押的erc20的合约地址
        // const erc20InstanceB = 0x3048455f0aa79f1358c5948a2b560092e80630f3;
        //部署这钱包地址
        // console.log(erc20InstanceA.constructor.class_defaults);
        //部署合约地址
        console.log("奖励的erc20合约地址A", erc20InstanceA.address);
        console.log("质押的erc20合约地址B", erc20InstanceB.address);
        // console.log("钱包地址：", erc20InstanceA.constructor.class_defaults);
        const instance = await GinPledge.deployed(
            erc20InstanceA.address,
            erc20InstanceA.constructor.class_defaults.from,
            "100000000",
            "10000000",
            "100000000",
            "10000000",
            1634023662,
            1936702062,
            erc20InstanceB.address,
        );
        console.log("质押合约地址：", instance.address);
        //开启质押合约
        await instance.changeIsDIS(true);
        let beforeDis = await instance.isDIS.call();
        console.log("质押合约是否开启状态", beforeDis);
        // console.log("合约结束时间", String(await instance.endTime.call()));

        let recharge = 10000000000000;
        //往质押合约充值tokenA
        await erc20InstanceA.transfer(instance.address, recharge);
        let pledgeBalanceA = await erc20InstanceA.balanceOf.call(instance.address);
        console.log("质押合约余额A", String(pledgeBalanceA));

        //质押数量
        let pledgeValue = 1230000;
        //授权给质押合约
        await erc20InstanceB.approve(instance.address, pledgeValue);

        //获取质押的erc20地址
        let pledgeTokenERC = await instance.pledgeTokenERC.call();
        console.log("从质押获取质押的erc20合约地址", pledgeTokenERC);
        console.log("获取结束时间：", String(await instance.endTime.call()));
        console.log("从质押和玉获取奖励的erc20合约地址：", await instance.aaa.call());

        //进行质押
        await instance.pledgeToken(pledgeValue, erc20InstanceB.address);
        let pledgeBalanceB = await erc20InstanceB.balanceOf.call(instance.address);
        console.log("质押合约余额B", String(pledgeBalanceB));
        let getTotalPledge = await instance.getTotalPledge.call();
        console.log("获取质押总量：", String(getTotalPledge));
    });
});
